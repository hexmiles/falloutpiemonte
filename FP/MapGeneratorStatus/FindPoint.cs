﻿using System;

namespace FP
{
    public partial class MapGeneratorStatus
    {
        public (int x, int y) TranslateCoords(int x, int y)
        {
            float kHrz = PentagonHorizScale * Scale * 0.5f;
            float kVrt = PentagonVertScale * Scale;

            var worldXtrunc = Math.Truncate(x / kHrz) + mappa.MinX;

            var scaledY = y / kVrt;
            var yTrunc = Math.Truncate(scaledY);
            var yRemind = scaledY - yTrunc;
            var worldYtrunc = mappa.MaxY - yTrunc;
            int worldY = (int)worldYtrunc;

            var isYodd = (worldYtrunc % 2) == 1;
            var isXodd = (worldXtrunc % 2) == 1;

            var ajustY =
                yRemind > PentagonVertScale ?
                    -1
                : 
                    yRemind < (1 - PentagonVertScale) ? 
                        +1
                    :
                        0
            ;

            if (ajustY != 0)
            {
                if (isYodd == isXodd)
                    worldY += ajustY;
            }

            // x/y -> cella dovrebbe stare qui o in repository?
            return ((int)worldXtrunc, worldY);
        }
    }
}
