﻿using System;

namespace FP
{
    public partial class MapGeneratorStatus
    {
        public MapGeneratorStatus(Repository.Mappa pMappa)
        {
            mappa = pMappa;
        }

        static MapGeneratorStatus()
        {
            PentagonHorizScale = (float)Math.Sin(Math.PI / 3);
        }
    }
}
