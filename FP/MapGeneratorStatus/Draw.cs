﻿using System;
using System.Drawing;

namespace FP
{
    public partial class MapGeneratorStatus
    {
        public Bitmap DrawMap()
        {
            var listCelle = mappa.GetCelle();

            int bmpWidth = (Scale * (1 + mappa.MaxX - mappa.MinX) + 1) / 2;
            int bmpHeight = (int)((Scale * (1 + mappa.MaxY - mappa.MinY) + 1) * PentagonVertScale);

            float kHrz = PentagonHorizScale * Scale * 0.5f;
            float kVrt = PentagonVertScale* Scale;

            var bmp = new Bitmap(bmpWidth, bmpHeight);

            using (var gr = Graphics.FromImage(bmp))
            using (var fo = new Font(FontFamily, FontSize))
            using (var blackPen = new Pen(Color.Black))
            {
                foreach (var itemcella in listCelle)
                {
                    Brush fillBrush;
                    Brush textBrush = Brushes.Black;

                    var coord_x = itemcella.x - mappa.MinX;
                    var coord_y = mappa.MaxY - itemcella.y;
                    var scaled_x = kHrz * (1 + coord_x);
                    var scaled_y = kVrt * (0.5f + coord_y);

                    var points = GeneratePentagon(scaled_x, scaled_y);

                    if (itemcella.x == 0 && itemcella.y == 0)
                    {
                        fillBrush = Brushes.Black;
                        textBrush = Brushes.White;
                    }
                    else
                    {
                        switch (itemcella.colore)
                        {
                            case "rosso": fillBrush = Brushes.Red; break;
                            case "verde": fillBrush = Brushes.Green; break;
                            case "blu": fillBrush = Brushes.Blue; break;
                            case "viola": fillBrush = Brushes.Violet; break;
                            case "fucsia": fillBrush = Brushes.Fuchsia; break;
                            default: fillBrush = Brushes.White; break;
                        }
                    }

                    gr.FillPolygon(fillBrush, points);
                    gr.DrawPolygon(blackPen, points);
                    gr.DrawString(itemcella.ordinale.ToString(), fo, textBrush, scaled_x - 9, scaled_y - 7);
                }

                // penso che dovrei dividere questa cosa in più layout di bitmap
                // così evito di ridisegnare la mappa se non serve
                if (SelectedCell.HasValue)
                {
                    var coord_x = SelectedCell.Value.X - mappa.MinX;
                    var coord_y = mappa.MaxY - SelectedCell.Value.Y;
                    var scaled_x = PentagonHorizScale * (Scale / 2 + Scale * coord_x / 2);
                    var scaled_y = PentagonVertScale * (Scale / 2 + Scale * coord_y);

                    var points = GeneratePentagon(scaled_x, scaled_y);

                    using (var selPen = new Pen(Color.Black, 3))
                        gr.DrawPolygon(selPen, points);
                }
            }

            return bmp;
        }

        private PointF[] GeneratePentagon(float scaledX, float scaledY)
        {
            var points = new PointF[6];

            for (int alpha = 0; alpha < 6; ++alpha)
            {
                var angle = Math.PI * alpha * 60 / 180;
                points[alpha] = new PointF(
                    scaledX + (float)(0.5 * Scale * Math.Sin(angle)),
                    scaledY + (float)(0.5 * Scale * Math.Cos(angle))
                );
            }

            return points;
        }
    }
}
