﻿namespace FP
{
    public partial class MapGeneratorStatus
    {
        private Repository.Mappa mappa;

        // potrebbe essere un array?
        public (int X, int Y)? SelectedCell = null;
        public int Scale = 22;

        static public float PentagonHorizScale;
        static public float PentagonVertScale = 0.75f;

        public string FontFamily = "Arial";
        public float FontSize = 8;
    }
}
