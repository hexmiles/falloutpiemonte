﻿namespace FP
{
    partial class frmFP
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbMappa = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.vuotoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabMappa = new System.Windows.Forms.TabPage();
            this.mapTools = new FP.ucMapTools();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            ((System.ComponentModel.ISupportInitialize)(this.pbMappa)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tabMappa.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbMappa
            // 
            this.pbMappa.BackColor = System.Drawing.Color.White;
            this.pbMappa.Location = new System.Drawing.Point(0, 0);
            this.pbMappa.Name = "pbMappa";
            this.pbMappa.Size = new System.Drawing.Size(125, 93);
            this.pbMappa.TabIndex = 1;
            this.pbMappa.TabStop = false;
            this.pbMappa.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbMappa_MouseClick);
            this.pbMappa.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PbMappa_MouseMove);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vuotoToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // vuotoToolStripMenuItem
            // 
            this.vuotoToolStripMenuItem.Name = "vuotoToolStripMenuItem";
            this.vuotoToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.vuotoToolStripMenuItem.Text = "Menu";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // tabMain
            // 
            this.tabMain.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabMain.Controls.Add(this.tabMappa);
            this.tabMain.Controls.Add(this.tabPage2);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 24);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(800, 404);
            this.tabMain.TabIndex = 3;
            // 
            // tabMappa
            // 
            this.tabMappa.AutoScroll = true;
            this.tabMappa.BackColor = System.Drawing.Color.White;
            this.tabMappa.Controls.Add(this.mapTools);
            this.tabMappa.Controls.Add(this.pbMappa);
            this.tabMappa.Location = new System.Drawing.Point(4, 25);
            this.tabMappa.Name = "tabMappa";
            this.tabMappa.Padding = new System.Windows.Forms.Padding(3);
            this.tabMappa.Size = new System.Drawing.Size(792, 375);
            this.tabMappa.TabIndex = 0;
            this.tabMappa.Text = "Mappa";
            // 
            // mapTools
            // 
            this.mapTools.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mapTools.Location = new System.Drawing.Point(319, 55);
            this.mapTools.Name = "mapTools";
            this.mapTools.Size = new System.Drawing.Size(189, 249);
            this.mapTools.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(792, 375);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // frmFP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmFP";
            this.Text = "Fallout Piemonte";
            ((System.ComponentModel.ISupportInitialize)(this.pbMappa)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabMain.ResumeLayout(false);
            this.tabMappa.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pbMappa;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem vuotoToolStripMenuItem;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabMappa;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private ucMapTools mapTools;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

