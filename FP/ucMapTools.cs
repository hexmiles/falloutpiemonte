﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FP
{
    public partial class ucMapTools : UserControl
    {
        private bool isMoving = false;
        private Point movingMousePos;

        // ----------------

        public ucMapTools()
        {
            InitializeComponent();
        }

        private void UcMapTools_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMoving = true;
                movingMousePos = e.Location;
            }
        }

        private void UcMapTools_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMoving = false;
            }
        }

        private void UcMapTools_MouseMove(object sender, MouseEventArgs e){
            if (isMoving){
                Top += e.Y - movingMousePos.Y;
                Left += e.X - movingMousePos.X;
            }
        }

        private void TbScala_Scroll(object sender, EventArgs e){
            gpScala.Text = $"Scala ({tbScala.Value})";
            ScaleChanged?.Invoke(tbScala.Value);
        }

        public void WriteCoords(int x, int y, int settore, int ordinale, int pixelX, int pixelY){
            txtCellaXp.Text = pixelX.ToString();
            txtCellaYp.Text = pixelY.ToString();
            txtX.Text = x.ToString();
            txtY.Text = y.ToString();
            txtCellaSettore.Text = settore.ToString();
            txtCellaOrdinale.Text = ordinale.ToString();
        }

        public Action<int> ScaleChanged = null;

        public void SetSelezionata(int settore, int ordinale){
            txtSelOrdinale.Text = ordinale.ToString();
            txtSelSettore.Text = settore.ToString();
        }
    }
}
