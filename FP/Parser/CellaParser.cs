﻿using FP.Models;
using System.Xml;

namespace FP.Parser
{
    public partial class CellaParser
    {
        static public Cella FromXml(XmlNode nodoCella)
        {
            var parsedCella = new Cella();
            parsedCella.x = int.Parse(nodoCella.Attributes["x"].Value);
            parsedCella.y = int.Parse(nodoCella.Attributes["y"].Value);
            parsedCella.ordinale = int.Parse(nodoCella.Attributes["ordinale"].Value);
            parsedCella.settore = nodoCella.ParentNode.Attributes["nome"].Value;
            parsedCella.colore = nodoCella.ParentNode.Attributes["colore"].Value;

            return parsedCella;
        }
    }
}
