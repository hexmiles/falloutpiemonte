﻿using FP.Models;
using System;
using System.Collections.Generic;
using System.Xml;

namespace FP.Repository
{
    public class Mappa
    {
        // potrebbero essere float
        public int MaxX = int.MinValue;
        public int MaxY = int.MinValue;
        public int MinX = int.MaxValue;
        public int MinY = int.MaxValue;

        private Cella[] celle = null;

        // il repository dovrebbe usare un driver per l'accesso all'xml
        public void FakeInitDriver(string xmlDocPath)
        {
            var xdoc = new XmlDocument();
            xdoc.Load(xmlDocPath);

            var nodiCella = xdoc.SelectNodes("//cella");
            var list = new List<Cella>();

            foreach (XmlNode nodoCella in nodiCella)
            {
                var parsedCella = Parser.CellaParser.FromXml(nodoCella);
                list.Add(parsedCella);

                if (parsedCella.x > MaxX) MaxX = parsedCella.x;
                if (parsedCella.y > MaxY) MaxY = parsedCella.y;
                if (parsedCella.x < MinX) MinX = parsedCella.x;
                if (parsedCella.y < MinY) MinY = parsedCella.y;
            }

            celle = list.ToArray();
        }

        public Cella[] GetCelle() => celle;

        public void VerificaCoordinate()
        {
            var celle = new Cella[2 + MaxY - MinY, 2 + MaxX - MinX];

            foreach(var itemcella in celle)
            {
                var coord_x = itemcella.x - MinX;
                var coord_y = itemcella.y - MinY;
            
                if (celle[coord_y, coord_x] != null)
                {
                    throw new Exception("trovato errore in coordinate");
                }
                else
                {
                    celle[coord_y, coord_x] = itemcella;
                }
            }
        }

        public void VerificaOrdinali()
        {
            var settori = new Dictionary<string, HashSet<int>>();
            
            foreach (var itemcella in celle)
            {
                if (!settori.ContainsKey(itemcella.settore))
                    settori.Add(itemcella.settore, new HashSet<int>());

                if (settori[itemcella.settore].Contains(itemcella.ordinale))
                    throw new Exception("trovato errore in ordinali");
                else
                    settori[itemcella.settore].Add(itemcella.ordinale);
            }
        }
    }
}
