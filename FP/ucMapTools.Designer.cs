﻿namespace FP
{
    partial class ucMapTools
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbScala = new System.Windows.Forms.TrackBar();
            this.txtCellaXp = new System.Windows.Forms.TextBox();
            this.lblCellaXp = new System.Windows.Forms.Label();
            this.lblCellaYp = new System.Windows.Forms.Label();
            this.txtCellaYp = new System.Windows.Forms.TextBox();
            this.gpCoord = new System.Windows.Forms.GroupBox();
            this.txtCellaOrdinale = new System.Windows.Forms.TextBox();
            this.lblOrdinale = new System.Windows.Forms.Label();
            this.txtCellaSettore = new System.Windows.Forms.TextBox();
            this.lblSettore = new System.Windows.Forms.Label();
            this.gpScala = new System.Windows.Forms.GroupBox();
            this.lblX = new System.Windows.Forms.Label();
            this.txtX = new System.Windows.Forms.TextBox();
            this.lblY = new System.Windows.Forms.Label();
            this.txtY = new System.Windows.Forms.TextBox();
            this.gpSelected = new System.Windows.Forms.GroupBox();
            this.lblSelSettore = new System.Windows.Forms.Label();
            this.txtSelSettore = new System.Windows.Forms.TextBox();
            this.lblSelOrdinale = new System.Windows.Forms.Label();
            this.txtSelOrdinale = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tbScala)).BeginInit();
            this.gpCoord.SuspendLayout();
            this.gpScala.SuspendLayout();
            this.gpSelected.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbScala
            // 
            this.tbScala.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbScala.Location = new System.Drawing.Point(3, 16);
            this.tbScala.Maximum = 50;
            this.tbScala.Minimum = 5;
            this.tbScala.Name = "tbScala";
            this.tbScala.Size = new System.Drawing.Size(175, 40);
            this.tbScala.TabIndex = 2;
            this.tbScala.Value = 22;
            this.tbScala.Scroll += new System.EventHandler(this.TbScala_Scroll);
            // 
            // txtCellaXp
            // 
            this.txtCellaXp.Location = new System.Drawing.Point(26, 15);
            this.txtCellaXp.Name = "txtCellaXp";
            this.txtCellaXp.ReadOnly = true;
            this.txtCellaXp.Size = new System.Drawing.Size(30, 20);
            this.txtCellaXp.TabIndex = 5;
            this.txtCellaXp.Text = "100";
            // 
            // lblCellaXp
            // 
            this.lblCellaXp.AutoSize = true;
            this.lblCellaXp.Location = new System.Drawing.Point(6, 18);
            this.lblCellaXp.Name = "lblCellaXp";
            this.lblCellaXp.Size = new System.Drawing.Size(20, 13);
            this.lblCellaXp.TabIndex = 6;
            this.lblCellaXp.Text = "Xp";
            // 
            // lblCellaYp
            // 
            this.lblCellaYp.AutoSize = true;
            this.lblCellaYp.Location = new System.Drawing.Point(6, 44);
            this.lblCellaYp.Name = "lblCellaYp";
            this.lblCellaYp.Size = new System.Drawing.Size(20, 13);
            this.lblCellaYp.TabIndex = 7;
            this.lblCellaYp.Text = "Yp";
            // 
            // txtCellaYp
            // 
            this.txtCellaYp.Location = new System.Drawing.Point(26, 41);
            this.txtCellaYp.Name = "txtCellaYp";
            this.txtCellaYp.ReadOnly = true;
            this.txtCellaYp.Size = new System.Drawing.Size(30, 20);
            this.txtCellaYp.TabIndex = 8;
            this.txtCellaYp.Text = "100";
            // 
            // gpCoord
            // 
            this.gpCoord.Controls.Add(this.txtY);
            this.gpCoord.Controls.Add(this.lblY);
            this.gpCoord.Controls.Add(this.txtX);
            this.gpCoord.Controls.Add(this.lblX);
            this.gpCoord.Controls.Add(this.txtCellaOrdinale);
            this.gpCoord.Controls.Add(this.lblOrdinale);
            this.gpCoord.Controls.Add(this.txtCellaSettore);
            this.gpCoord.Controls.Add(this.lblSettore);
            this.gpCoord.Controls.Add(this.lblCellaXp);
            this.gpCoord.Controls.Add(this.txtCellaYp);
            this.gpCoord.Controls.Add(this.txtCellaXp);
            this.gpCoord.Controls.Add(this.lblCellaYp);
            this.gpCoord.Location = new System.Drawing.Point(3, 65);
            this.gpCoord.Name = "gpCoord";
            this.gpCoord.Size = new System.Drawing.Size(181, 73);
            this.gpCoord.TabIndex = 9;
            this.gpCoord.TabStop = false;
            this.gpCoord.Text = "Coordinate";
            // 
            // txtCellaOrdinale
            // 
            this.txtCellaOrdinale.Location = new System.Drawing.Point(146, 41);
            this.txtCellaOrdinale.Name = "txtCellaOrdinale";
            this.txtCellaOrdinale.ReadOnly = true;
            this.txtCellaOrdinale.Size = new System.Drawing.Size(29, 20);
            this.txtCellaOrdinale.TabIndex = 12;
            this.txtCellaOrdinale.Text = "120";
            // 
            // lblOrdinale
            // 
            this.lblOrdinale.AutoSize = true;
            this.lblOrdinale.Location = new System.Drawing.Point(113, 44);
            this.lblOrdinale.Name = "lblOrdinale";
            this.lblOrdinale.Size = new System.Drawing.Size(27, 13);
            this.lblOrdinale.TabIndex = 11;
            this.lblOrdinale.Text = "Ord.";
            // 
            // txtCellaSettore
            // 
            this.txtCellaSettore.Location = new System.Drawing.Point(146, 15);
            this.txtCellaSettore.Name = "txtCellaSettore";
            this.txtCellaSettore.ReadOnly = true;
            this.txtCellaSettore.Size = new System.Drawing.Size(22, 20);
            this.txtCellaSettore.TabIndex = 10;
            this.txtCellaSettore.Text = "12";
            // 
            // lblSettore
            // 
            this.lblSettore.AutoSize = true;
            this.lblSettore.Location = new System.Drawing.Point(114, 18);
            this.lblSettore.Name = "lblSettore";
            this.lblSettore.Size = new System.Drawing.Size(26, 13);
            this.lblSettore.TabIndex = 9;
            this.lblSettore.Text = "Set.";
            // 
            // gpScala
            // 
            this.gpScala.Controls.Add(this.tbScala);
            this.gpScala.Location = new System.Drawing.Point(3, 0);
            this.gpScala.Name = "gpScala";
            this.gpScala.Size = new System.Drawing.Size(181, 59);
            this.gpScala.TabIndex = 10;
            this.gpScala.TabStop = false;
            this.gpScala.Text = "Scala (22)";
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(62, 18);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(14, 13);
            this.lblX.TabIndex = 13;
            this.lblX.Text = "X";
            // 
            // txtX
            // 
            this.txtX.Location = new System.Drawing.Point(78, 15);
            this.txtX.Name = "txtX";
            this.txtX.ReadOnly = true;
            this.txtX.Size = new System.Drawing.Size(30, 20);
            this.txtX.TabIndex = 14;
            this.txtX.Text = "100";
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Location = new System.Drawing.Point(62, 44);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(14, 13);
            this.lblY.TabIndex = 15;
            this.lblY.Text = "Y";
            // 
            // txtY
            // 
            this.txtY.Location = new System.Drawing.Point(77, 41);
            this.txtY.Name = "txtY";
            this.txtY.ReadOnly = true;
            this.txtY.Size = new System.Drawing.Size(30, 20);
            this.txtY.TabIndex = 16;
            this.txtY.Text = "100";
            // 
            // gpSelected
            // 
            this.gpSelected.Controls.Add(this.label1);
            this.gpSelected.Controls.Add(this.txtSelOrdinale);
            this.gpSelected.Controls.Add(this.lblSelOrdinale);
            this.gpSelected.Controls.Add(this.txtSelSettore);
            this.gpSelected.Controls.Add(this.lblSelSettore);
            this.gpSelected.Location = new System.Drawing.Point(3, 144);
            this.gpSelected.Name = "gpSelected";
            this.gpSelected.Size = new System.Drawing.Size(181, 97);
            this.gpSelected.TabIndex = 11;
            this.gpSelected.TabStop = false;
            this.gpSelected.Text = "Cella Selezionata";
            // 
            // lblSelSettore
            // 
            this.lblSelSettore.AutoSize = true;
            this.lblSelSettore.Location = new System.Drawing.Point(6, 22);
            this.lblSelSettore.Name = "lblSelSettore";
            this.lblSelSettore.Size = new System.Drawing.Size(41, 13);
            this.lblSelSettore.TabIndex = 17;
            this.lblSelSettore.Text = "Settore";
            // 
            // txtSelSettore
            // 
            this.txtSelSettore.Location = new System.Drawing.Point(52, 19);
            this.txtSelSettore.Name = "txtSelSettore";
            this.txtSelSettore.ReadOnly = true;
            this.txtSelSettore.Size = new System.Drawing.Size(22, 20);
            this.txtSelSettore.TabIndex = 17;
            this.txtSelSettore.Text = "12";
            // 
            // lblSelOrdinale
            // 
            this.lblSelOrdinale.AutoSize = true;
            this.lblSelOrdinale.Location = new System.Drawing.Point(80, 22);
            this.lblSelOrdinale.Name = "lblSelOrdinale";
            this.lblSelOrdinale.Size = new System.Drawing.Size(46, 13);
            this.lblSelOrdinale.TabIndex = 17;
            this.lblSelOrdinale.Text = "Ordinale";
            // 
            // txtSelOrdinale
            // 
            this.txtSelOrdinale.Location = new System.Drawing.Point(132, 19);
            this.txtSelOrdinale.Name = "txtSelOrdinale";
            this.txtSelOrdinale.ReadOnly = true;
            this.txtSelOrdinale.Size = new System.Drawing.Size(29, 20);
            this.txtSelOrdinale.TabIndex = 17;
            this.txtSelOrdinale.Text = "120";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Terreni";
            // 
            // ucMapTools
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.gpSelected);
            this.Controls.Add(this.gpCoord);
            this.Controls.Add(this.gpScala);
            this.Name = "ucMapTools";
            this.Size = new System.Drawing.Size(189, 249);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.UcMapTools_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.UcMapTools_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.UcMapTools_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.tbScala)).EndInit();
            this.gpCoord.ResumeLayout(false);
            this.gpCoord.PerformLayout();
            this.gpScala.ResumeLayout(false);
            this.gpScala.PerformLayout();
            this.gpSelected.ResumeLayout(false);
            this.gpSelected.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TrackBar tbScala;
        private System.Windows.Forms.TextBox txtCellaXp;
        private System.Windows.Forms.Label lblCellaXp;
        private System.Windows.Forms.Label lblCellaYp;
        private System.Windows.Forms.TextBox txtCellaYp;
        private System.Windows.Forms.GroupBox gpCoord;
        private System.Windows.Forms.TextBox txtCellaOrdinale;
        private System.Windows.Forms.Label lblOrdinale;
        private System.Windows.Forms.TextBox txtCellaSettore;
        private System.Windows.Forms.Label lblSettore;
        private System.Windows.Forms.GroupBox gpScala;
        private System.Windows.Forms.TextBox txtY;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.TextBox txtX;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.GroupBox gpSelected;
        private System.Windows.Forms.TextBox txtSelSettore;
        private System.Windows.Forms.Label lblSelSettore;
        private System.Windows.Forms.TextBox txtSelOrdinale;
        private System.Windows.Forms.Label lblSelOrdinale;
        private System.Windows.Forms.Label label1;
    }
}
