﻿using System;
using System.Windows.Forms;

namespace FP
{
    public partial class frmFP : Form
    {
        private Repository.Mappa mappa;
        private MapGeneratorStatus mgs;

        // -------------

        public frmFP()
        {
            mappa = new Repository.Mappa();
            mappa.FakeInitDriver("mappa.xml");

            mgs = new MapGeneratorStatus(mappa);

            InitializeComponent();

            mapTools.ScaleChanged = ScaleChanged;

            var bmp = mgs.DrawMap();
            pbMappa.Image = bmp;

            pbMappa.Width = bmp.Width;
            pbMappa.Height = bmp.Height;
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("https://gitlab.com/ontorder/falloutpiemonte", "ti prego usami");
        }

        private void PbMappa_MouseClick(object sender, MouseEventArgs e)
        {
            var qualcosa = mgs.TranslateCoords(e.X, e.Y);
            mapTools.SetSelezionata(0, 0);
        }

        private void PbMappa_MouseMove(object sender, MouseEventArgs e)
        {
            var worldCoord = mgs.TranslateCoords(e.X, e.Y);
            mapTools.WriteCoords(worldCoord.x, worldCoord.y, 0, 0, e.X, e.Y);
        }

        private void ScaleChanged(int scale)
        {
            mgs.Scale = scale;

            var bmp = mgs.DrawMap();
            pbMappa.Image = bmp;

            pbMappa.Width = bmp.Width;
            pbMappa.Height = bmp.Height;
        }
    }
}
